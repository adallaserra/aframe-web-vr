<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>360&deg; Image Gallery</title>
    <meta name="description" content="360&deg; Image Gallery - A-Frame">
    <script src="https://aframe.io/releases/0.5.0/aframe.min.js"></script>
    <script src="https://npmcdn.com/aframe-animation-component@3.0.1"></script>
    <script src="https://npmcdn.com/aframe-event-set-component@3.0.1"></script>
    <script src="https://npmcdn.com/aframe-layout-component@3.0.1"></script>
    <script src="https://npmcdn.com/aframe-template-component@3.1.1"></script>
    <script src="components/set-image.js"></script>
  </head>
  <body>
    <a-scene>
      <a-assets>
		  <img id="antennas-thumb" crossorigin="anonymous" src="buttons/antennas-button.jpg">
		  <img id="boats-thumb" crossorigin="anonymous" src="buttons/boats-button.jpg">
		  <img id="sunset-thumb" crossorigin="anonymous" src="buttons/sunset-button.jpg">
		  <img id="porsche-thumb" crossorigin="anonymous" src="buttons/porsche-button.jpg">
		  <audio id="click-sound" crossorigin="anonymous" src="click.ogg"></audio>
		  <img id="antennas" crossorigin="anonymous" src="VR_Images/antennas.jpg">
		  <img id="boats" crossorigin="anonymous" src="VR_Images/boats.jpg">
		  <img id="sunset" crossorigin="anonymous" src="VR_Images/sunset.jpg">
		  <img id="porsche" crossorigin="anonymous" src="VR_Images/porsche.jpg">

        <!-- Image link template to be reused. -->
        <script id="link" type="text/html">
          <a-entity class="link"
            geometry="primitive: plane; height: 1; width: 1"
            material="shader: flat; src: ${thumb}"
            event-set__1="_event: mousedown; scale: 1 1 1"
            event-set__2="_event: mouseup; scale: 1.2 1.2 1"
            event-set__3="_event: mouseenter; scale: 1.2 1.2 1"
            event-set__4="_event: mouseleave; scale: 1 1 1"
            set-image="on: click; target: #image-360; src: ${src}"
            sound="on: click; src: #click-sound"></a-entity>
        </script>
      </a-assets>

      <!-- 360-degree image. -->
      <a-sky id="image-360" radius="10" src="#porsche"></a-sky>

      <!-- Image links. -->
      <a-entity id="links" layout="type: line; margin: 1.5" position="-2.25 0 -4">
		  <a-entity template="src: #link" data-src="#porsche" data-thumb="#porsche-thumb"></a-entity>
		  <a-entity template="src: #link" data-src="#sunset" data-thumb="#sunset-thumb"></a-entity>
		  <a-entity template="src: #link" data-src="#boats" data-thumb="#boats-thumb"></a-entity>
		  <a-entity template="src: #link" data-src="#antennas" data-thumb="#antennas-thumb"></a-entity>
      </a-entity>

      <!-- Camera + cursor. -->
      <a-entity camera look-controls>
        <a-cursor id="cursor"
          animation__click="property: scale; startEvents: click; from: 0.1 0.1 0.1; to: 1 1 1; dur: 150"
          animation__fusing="property: fusing; startEvents: fusing; from: 1 1 1; to: 0.1 0.1 0.1; dur: 1500"
          event-set__1="_event: mouseenter; color: springgreen"
          event-set__2="_event: mouseleave; color: black"
          fuse="true"
          raycaster="objects: .link"></a-cursor>
      </a-entity>
    </a-scene>
  </body>
</html>